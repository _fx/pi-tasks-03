#include <stdio.h>
#include <time.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>

const int N = 20;

double arr[N];

double get(char cc){ // Через символ узнаем, что вернуть: для / - средн.арифм, для 'm' - минимум и для 'M'- максимум

	int i;
	double sum = 0, minimum = 0, maximum = 0;
	
	// Средн. арифм.
	if(cc == '/'){
		for(i = 0; i<N; i++)
			sum+=arr[i];
 		return sum/N;	
	}
	// Минимум
	if(cc == 'm'){
		minimum = 100000000;
		for(i = 0; i<N; i++)
			if(arr[i]<minimum) minimum=arr[i];
		return minimum;	
	}
	// Максимум
	if(cc == 'M'){
		maximum = -100000000;
		for(i = 0; i<N; i++)
			if(arr[i]>maximum) maximum=arr[i];
		return maximum;
	}

	return 0;
}

      
int main()
{       
	int i;
		
	srand(time(0));

	for(i=0; i<N; i++)
		arr[i] = rand()%10, 
		printf("%.0f ", arr[i]);

	printf("\nMin = %.0f\nMax = %.0f\nArifm = %.0f",get('m'),get('M'),get('/'));
	                                
	return 0;	        
}

                      