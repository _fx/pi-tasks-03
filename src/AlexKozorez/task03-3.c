#include <stdio.h>
#include <time.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>

int letters[26]; // Массив для букв которые имеет номера от 97 до 122; 
      
int main()
{       
    char buf[256];
	int size, i, pos;
	
	fgets(buf,256,stdin);
	size = strlen(buf);
	buf[size]='\0';	            
	
	for(i = 0; i<size; i++)
	{
 	    pos = ((int) buf[i]) - 97;
		letters[pos]++;
 	}

	for(i = 0; i<26; i++)
	{ 
		if(letters[i]>0){ 
			printf("%c-%d ",i+97,letters[i]);
		}
	}
                                              
	return 0;
		        
}                      